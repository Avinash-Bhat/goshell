package goshell

// Command a command interface
type Command interface {
	// TODO add io streams to the
	// Run run the command
	Run(command string)

	Key() string
}

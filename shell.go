package goshell

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// Shell a shell instance
type Shell struct {
	env      *Env
	commands []Command
}

// NewShell creates a new shell
func NewShell(commands ...Command) (shell *Shell, err error) {
	e, err := NewEnv()
	if err != nil {
		return nil, err
	}
	shell = &Shell{e, commands}
	return
}

func (shell *Shell) ps1() string {
	return shell.env.Ps1()
}

// Loop loops the shell with input
func (shell *Shell) Loop() {
	fmt.Print(shell.ps1())
	scanner := bufio.NewScanner(os.Stdin)
	for {
		if !scanner.Scan() {
			continue
		}
		command := strings.TrimSpace(scanner.Text())
		if command == "" {
			fmt.Print(shell.ps1())
			continue
		}
		shell.process(command)
		fmt.Print(shell.ps1())
	}
}

func (shell *Shell) process(commandStr string) {
	// use a radix tree like in httprouter for faster processing.
	for i := 0; i < len(shell.commands); i++ {
		command := shell.commands[i]
		if strings.HasPrefix(commandStr, command.Key()) {
			command.Run(commandStr)
			return
		}
	}
	fmt.Println("git: command not found: " + commandStr)
}

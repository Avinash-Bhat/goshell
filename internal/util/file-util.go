package util

import (
	"os"
	"path/filepath"
)

// Pwd get the present working directory
func Pwd() (string, error) {
	return filepath.Abs(filepath.Dir(os.Args[0]))
}

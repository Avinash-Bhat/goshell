package goshell

import (
	"gitlab.com/Avinash-Bhat/goshell/internal/util"
)

// Env the environment struct
type Env struct {
	pwd   string
	ps1   string
	debug bool
}

// NewEnv create a new environment
func NewEnv() (e *Env, err error) {
	e = &Env{}
	err = e.Init()
	return e, err
}

// Init init the environment
func (e *Env) Init() (err error) {
	e.pwd, err = util.Pwd()
	// TODO make this configurable
	e.ps1 = "git> "
	return
}

/*
* GETTERS SECTION
 */

// Pwd get the current working directory
func (e *Env) Pwd() string {
	return e.pwd
}

// Ps1 get the PS1 variable
func (e *Env) Ps1() string {
	return e.ps1
}

/*
* GETTERS SECTION END
 */
